package com.headlessideas

import com.headlessideas.inferit.inferMovie
import org.slf4j.LoggerFactory


val testMovies = listOf(
    "Die Hard 1988 1080p BluRay Remux AVC DTS-HD MA 5.1 - KRaLiMaRKo.mkv",
    "The.Hobbit.The.Desolation.Of.Smaug.2013.1080p.BluRay.DTS.x264-HDMaNiAcS.mkv"
)

private val logger = LoggerFactory.getLogger("TestEpisode")!!

fun main() {
    //fullMovieTest()
    singleMovieTest()
}

fun fullMovieTest() {
    testMovies.forEach {
        logger.info("Testing: $it")
        logger.info("${it.inferMovie()}")
        logger.info("")
    }
}

fun singleMovieTest() {
    val test = "A Nightmare on Elm Street (1984).mkv"
    logger.info("${test.inferMovie()}")
}
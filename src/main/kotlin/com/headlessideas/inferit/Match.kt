package com.headlessideas.inferit

import com.headlessideas.inferit.properties.Property

internal class Match(private val property: Property<*>, regex: Regex, private var score: Double = 10.0, private val matchValue: Any? = null) {
    private var quality: Int = 0
    private var regexList = listOf(regex)
    var requireProperty: Property<*>? = null
        private set
    var requirePropertyValue: String? = null
        private set
    var positionBeforeProperty: Property<*>? = null
        private set
    var positionAfterProperty: Property<*>? = null
        private set

    fun require(property: Property<*>, value: String? = null): Match {
        requireProperty = property
        requirePropertyValue = value
        return this
    }

    fun positionBefore(property: Property<*>): Match {
        positionBeforeProperty = property
        return this
    }

    fun positionAfter(property: Property<*>): Match {
        positionAfterProperty = property
        return this
    }

    fun findMatch(source: String, toEdge: Boolean): MatchStats? {

        var matches: MatchResult? = null

        var score = this.score

        for ((i, regex) in regexList.withIndex()) {
            // If our string is not to the edge, yet this regex looks for the end, then skip it
            if (!toEdge && regex.pattern.contains('$')) continue

            matches = regex.find(source) ?: continue

            score -= (i / 10.0)
            break
        }

        if (matches == null) return null

        val value = matchValue?.toString() ?: matches.groupValues[1]
        return MatchStats(score, quality, matches, property.parse(value))
    }
}

internal class MatchStats(var score: Double = 0.0, val quality: Int = 0, val matches: MatchResult, val result: Any?) {
    var matcher: Match? = null
    var extract: String? = null
    val begin
        get() = matches.range.first
    val end
        get() = matches.range.last

    override fun toString(): String {
        return result?.toString() ?: ""
    }
}
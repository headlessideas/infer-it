package com.headlessideas.inferit.episode

import com.headlessideas.inferit.AbstractParser
import com.headlessideas.inferit.MatchStats
import com.headlessideas.inferit.episode.EpisodeConfig.configDateProperty
import com.headlessideas.inferit.episode.EpisodeConfig.configEpisodeProperty
import com.headlessideas.inferit.episode.EpisodeConfig.configSeasonProperty
import com.headlessideas.inferit.episode.EpisodeConfig.configSeriesProperty
import com.headlessideas.inferit.episode.EpisodeConfig.configSeriesYearProperty
import com.headlessideas.inferit.episode.EpisodeConfig.configTitleProperty
import com.headlessideas.inferit.properties.DateProperty
import com.headlessideas.inferit.properties.IntProperty
import com.headlessideas.inferit.properties.NameProperty
import com.headlessideas.inferit.properties.Property

internal class EpisodeParser(source: String) : AbstractParser(source) {
    private val dateProperty: DateProperty = configDateProperty()
    private val seasonProperty: IntProperty = configSeasonProperty()
    private val episodeProperty: IntProperty = configEpisodeProperty()
    private val titleProperty: NameProperty = configTitleProperty(dateProperty, episodeProperty)
    private val seriesYearProperty: IntProperty = configSeriesYearProperty(episodeProperty)
    private val seriesProperty: NameProperty = configSeriesProperty(seriesYearProperty, dateProperty, seasonProperty, episodeProperty)

    fun process(): EpisodeInformation {
        repeat(2) { pass ->
            processRequiredProperties(pass)
            processProperty(dateProperty, pass)
            processProperty(seriesProperty, pass)
            processProperty(seriesYearProperty, pass)
            processProperty(titleProperty, pass)
            processProperty(seasonProperty, pass)
            processProperty(episodeProperty, pass)
        }

        return buildEpisodeInformation()
    }

    private fun processProperty(property: Property<*>, pass: Int) {
        property
            .process(this, pass)
            .sortedByDescending { it.quality }
            .firstOrNull()?.let {
                property.marked = true
                property.match = it
            }
    }

    private fun processRequiredProperties(pass: Int) {
        if (!seasonProperty.marked) processProperty(seasonProperty, pass)
        if (!episodeProperty.marked) processProperty(episodeProperty, pass)
    }

    private fun buildEpisodeInformation(): EpisodeInformation {
        return EpisodeInformation(
            seriesProperty.match?.toString() ?: "",
            titleProperty.match?.toString() ?: "",
            seriesYearProperty.match?.result as Int?,
            seasonProperty.match?.result as Int?,
            episodeProperty.match?.result as Int?
        )
    }

    override fun matches(): List<MatchStats> {
        return listOfNotNull(
            dateProperty.match,
            seasonProperty.match,
            episodeProperty.match,
            titleProperty.match,
            seriesYearProperty.match,
            seriesProperty.match
        )
    }
}

data class EpisodeInformation(
    val series: String? = null,
    val title: String? = null,
    val seriesYear: Int? = null,
    val season: Int? = null,
    val episode: Int? = null
)
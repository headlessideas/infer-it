package com.headlessideas.inferit.episode

import com.headlessideas.inferit.properties.DateProperty
import com.headlessideas.inferit.properties.IntProperty
import com.headlessideas.inferit.properties.NameProperty
import kotlin.text.RegexOption.IGNORE_CASE


object EpisodeConfig {
    internal fun configDateProperty(): DateProperty {
        // Extract dates, they"re quite specific
        val date = DateProperty()

        date.addExtract(Regex("\\b(20\\d{6})\\b"), 80.0) // "20021107"
        date.addExtract(Regex("\\b(19\\d{6})\\b"), 80.0) // "19971107"
        date.addExtract(Regex("\\x28(20\\d{6})\\x29"), 80.0) // "(20021107)"
        date.addExtract(Regex("\\x28(19\\d{6})\\x29"), 80.0) // "(20021107)"
        date.addExtract(Regex("\\b(20\\d{2}-\\d{2}-\\d{2})\\b"), 80.0) // "2002-11-07"
        date.addExtract(Regex("\\b(19\\d{2}-\\d{2}-\\d{2})\\b"), 80.0) // "1997-11-07"
        date.addExtract(Regex("\\x28\\b(20\\d{2}-\\d{2}-\\d{2})\\b\\x29"), 80.0) // "(2002-11-07)"
        date.addExtract(Regex("\\x28\\b(19\\d{2}-\\d{2}-\\d{2})\\b\\x29"), 80.0) // "(1997-11-07)"
        date.addExtract(Regex("\\b(20\\d{2}\\.\\d{2}\\.\\d{2})\\b"), 80.0) // "2002.11.07
        date.addExtract(Regex("\\b(19\\d{2}\\.\\d{2}\\.\\d{2})\\b"), 80.0) // "1997.11.07
        date.addExtract(Regex("\\x28\\b(20\\d{2}\\.\\d{2}\\.\\d{2})\\b\\x29"), 80.0) // "(2002-11-07)"
        date.addExtract(Regex("\\x28\\b(19\\d{2}\\.\\d{2}\\.\\d{2})\\b\\x29"), 80.0) // "(1997-11-07)"
        date.addExtract(Regex("\\b(\\d{2}\\.\\d{2}\\.20\\d{2})\\b"), 80.0) // "07.11.2002"
        date.addExtract(Regex("\\b(\\d{2}\\.\\d{2}\\.19\\d{2})\\b"), 80.0) // "07.11.1997"
        date.addExtract(Regex("\\x28\\b(\\d{2}\\.\\d{2}\\.20\\d{2})\\b\\x29"), 80.0) // "(07.11.2002)"
        date.addExtract(Regex("\\x28\\b(\\d{2}\\.\\d{2}\\.19\\d{2})\\b\\x29"), 80.0) // "(07.11.1997)"
        date.addExtract(Regex("\\b(\\d{2}-\\d{2}-20\\d{2})\\b"), 80.0)  //"07-11-2002"
        date.addExtract(Regex("\\b(\\d{2}-\\d{2}-19\\d{2})\\b"), 80.0)  //"07-11-1997"
        date.addExtract(Regex("\\x28(\\d{2}-\\d{2}-20\\d{2})\\x29"), 80.0)  //"(07-11-2002)"
        date.addExtract(Regex("\\x28(\\d{2}-\\d{2}-19\\d{2})\\x29"), 80.0)  //"(07-11-1997)"

        return date
    }

    internal fun configSeriesProperty(seriesYear: IntProperty, date: DateProperty, season: IntProperty, episode: IntProperty): NameProperty {
        // The series name
        val series = NameProperty()
        series.addExtract(Regex("^([^-]+)\\WSeries", IGNORE_CASE), 15.0).require(episode).positionBefore(episode)
        series.addExtract(Regex("^([^-]+)", IGNORE_CASE), 15.0).require(episode).positionBefore(episode)
        series.addExtract(Regex("(.*)", IGNORE_CASE), 10.0).require(episode).positionBefore(episode)
        series.addExtract(Regex("(.*)", IGNORE_CASE), 10.0).require(episode).positionBefore(episode)
        series.addExtract(Regex("(.*)", IGNORE_CASE), 11.0).require(season).positionBefore(season)

        series.addExtract(Regex("(.*)", IGNORE_CASE), 20.0).require(seriesYear).positionBefore(seriesYear)
        series.addExtract(Regex("(.*)", IGNORE_CASE), 20.0).require(date).positionBefore(episode)
        series.addExtract(Regex("(.*)", IGNORE_CASE), 20.0).require(date).positionBefore(date)

        return series
    }

    internal fun configTitleProperty(date: DateProperty, episode: IntProperty): NameProperty {
        // The episode title
        val title = NameProperty()

        title.addModifier("length", 1)
        title.addExtract(Regex("^([^\\x28]+)", IGNORE_CASE), 10.0).require(date).positionAfter(date)
        title.addExtract(Regex("^([^\\x28]+)", IGNORE_CASE), 10.0).require(episode).positionAfter(episode)
        title.addExtract(Regex(" - (.+)", IGNORE_CASE), 41.0).require(episode).positionAfter(episode)
        title.addExtract(Regex(" - (.+) -", IGNORE_CASE), 45.0).require(episode).positionAfter(episode)
        title.addExtract(Regex(" - (.+)\$", IGNORE_CASE), 60.0).require(episode).positionAfter(episode)

        return title
    }

    internal fun configSeriesYearProperty(episode: IntProperty): IntProperty {
        val year = IntProperty()
        year.addExtract(Regex("\\x28(\\d{4})\\x29"), 60.0) // "(2012)"
            .positionBefore(episode)

        return year
    }

    internal fun configSeasonProperty(): IntProperty {
        val season = IntProperty()

        season.addExtract(Regex("\\b(19\\d\\d)\\W\\d+\\Wof\\W\\d+\\b", IGNORE_CASE), 80.0) // " 2012 14 of 21 "
        season.addExtract(Regex("\\b(20\\d\\d)\\W\\d+\\Wof\\W\\d+\\b", IGNORE_CASE), 80.0) // " 1997 14 of 21 "
        season.addExtract(Regex("s(\\d{1,2})e\\d{1,2}", IGNORE_CASE), 90.0) // "s01e01"
        season.addExtract(Regex("s(\\d{1,2})xe\\d{1,2}", IGNORE_CASE), 90.0) // "S01xE01"
        // "season 01 episode 01"
        season.addExtract(Regex("\\bseason\\W+(\\d{1,2})\\W+episode\\W+\\d{1,2}\\b", IGNORE_CASE), 80.0)
        season.addExtract(Regex("s(\\d{1,2})\\We\\d{1,2}", IGNORE_CASE), 80.0) // "s01 e01"
        season.addExtract(Regex("\\bS(\\d\\d)\\b"), 60.0) // " S01 "
        season.addExtract(Regex("\\b(\\d)x\\d\\d\\b"), 50.0) // " 1x01 "
        season.addExtract(Regex("\\b(\\d\\d)x\\d\\d\\b"), 50.0) // " 01x01 "
        season.addExtract(Regex("\\b(\\d)x\\d\\b"), 30.0) // " 1x1 "
        season.addExtract(Regex("(\\d\\d)x\\d\\d\\b"), 6.0) // "01x01 "
        season.addExtract(Regex("(\\d)x\\d\\b"), 5.0) // "1x1 "
        season.addExtract(Regex("(?=\\W)\\[(\\d)\\d\\d](?=\\W)"), 20.0) // " [101] "
        season.addExtract(Regex("\\b(\\d\\d)\\d\\d\\b"), 5.0) // " 1101 "
        season.addExtract(Regex("\\b(\\d)\\d\\d\\b"), 5.0) // " 101 "
        season.addExtract(Regex("\\b(\\d+)xAll\\b", IGNORE_CASE), 20.0) // " 1xAll "

        return season
    }

    internal fun configEpisodeProperty(): IntProperty {
        val episode = IntProperty()

        episode.addExtract(Regex("s\\d{1,2}e(\\d{1,2})", IGNORE_CASE), 90.0)         // "s01e01"
        episode.addExtract(Regex("s\\d{1,2}xe(\\d{1,2})", IGNORE_CASE), 90.0)        // "S01xE01"
        episode.addExtract(Regex("\\bseason\\W+\\d{1,2}\\W+episode\\W+(\\d{1,2})\\W", IGNORE_CASE), 80.0) // "season 01 episode 01"
        episode.addExtract(Regex("\\bepisode\\W+(\\d{1,3})\\W", IGNORE_CASE), 70.0) // "episode 01" or "episode 001"
        episode.addExtract(Regex("s\\d{1,2}\\We(\\d{1,2})", IGNORE_CASE), 80.0) // "s01 e01"
        episode.addExtract(Regex("\\b(\\d+)\\Wof\\W\\d+\\b", IGNORE_CASE), 60.0) // " 1 of 14 "
        episode.addExtract(Regex("e(\\d\\d\\d)", IGNORE_CASE), 60.0) // "e001"
        episode.addExtract(Regex("\\bE(\\d\\d)\\b"), 60.0) // " E01 "
        episode.addExtract(Regex("\\b\\dx(\\d\\d)\\b"), 50.0) // " 1x01 "
        episode.addExtract(Regex("\\b\\d\\dx(\\d\\d)\\b"), 50.0) // " 01x01 "
        episode.addExtract(Regex("\\b\\dx(\\d)\\b"), 30.0) // " 1x1 "
        episode.addExtract(Regex("(?=\\W)\\[\\d(\\d\\d)](?=\\W)"), 20.0) // " [101] "
        episode.addExtract(Regex("\\b\\d\\d(\\d\\d)\\b"), 7.0) // " 1101 "
        episode.addExtract(Regex("\\b\\d(\\d\\d)\\b"), 7.0) // " 101 "
        episode.addExtract(Regex("\\b(\\d\\d)\\b"), 5.0) // " 01 "
        episode.addExtract(Regex("\\b(\\d)\\b"), 3.0) // " 1 "
        episode.addExtract(Regex("\\b(\\d+)v\\d\\b"), 30.0) // " 1v1 "
        episode.addExtract(Regex("(?=\\W)-\\WEp\\W(\\d+)(?=\\W)"), 40.0) // " - Ep 21 "

        return episode
    }
}
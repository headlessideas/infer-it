package com.headlessideas.inferit

internal abstract class AbstractParser(val source: String) {
    abstract fun matches(): List<MatchStats>
}
package com.headlessideas.inferit

import com.headlessideas.inferit.episode.EpisodeInformation
import com.headlessideas.inferit.episode.EpisodeParser
import com.headlessideas.inferit.file.FileClassifier
import com.headlessideas.inferit.file.FileInformation
import com.headlessideas.inferit.file.FileType
import com.headlessideas.inferit.file.configFileTypes
import com.headlessideas.inferit.movie.MovieInformation
import com.headlessideas.inferit.movie.MovieParser
import java.nio.file.Paths

internal class InferIt {
    private val fileTypes: Set<FileClassifier> = configFileTypes()

    private fun processExtension(source: String, fileClassifiers: Set<FileClassifier>): FileInformation {
        return fileClassifiers
            .flatMap { it.getFileType(source) }
            .maxBy { it.score } ?: FileInformation(FileType.UNKNOWN, "", "", 0, 0)
    }


    fun parseEpisode(source: String): InferItResult<EpisodeInformation> {
        val path = Paths.get(source).toFile()
        val fileInfo = processExtension(path.name, fileTypes)
        val episodeInfo = EpisodeParser(path.nameWithoutExtension).process()
        return InferItResult(fileInfo, episodeInfo)
    }

    fun parseMovie(source: String): InferItResult<MovieInformation> {
        val path = Paths.get(source).toFile()
        val fileInfo = processExtension(path.name, fileTypes)
        val movieInfo = MovieParser(path.nameWithoutExtension).process()
        return InferItResult(fileInfo, movieInfo)
    }
}

fun String.inferEpisode(): InferItResult<EpisodeInformation> {
    return InferIt().parseEpisode(this)
}

fun String.inferMovie(): InferItResult<MovieInformation> {
    return InferIt().parseMovie(this)
}
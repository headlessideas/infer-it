package com.headlessideas.inferit

import com.headlessideas.inferit.file.FileInformation

data class InferItResult<T>(
    val fileInformation: FileInformation,
    val episodeInformation: T
)
package com.headlessideas.inferit.movie

import com.headlessideas.inferit.AbstractParser
import com.headlessideas.inferit.movie.MovieConfig.configTitleProperty
import com.headlessideas.inferit.movie.MovieConfig.configYearProperty
import com.headlessideas.inferit.MatchStats
import com.headlessideas.inferit.properties.IntProperty
import com.headlessideas.inferit.properties.NameProperty
import com.headlessideas.inferit.properties.Property

internal class MovieParser(source: String) : AbstractParser(source) {
    private val titleProperty: NameProperty = configTitleProperty()
    private val yearProperty: IntProperty = configYearProperty(titleProperty)

    fun process(): MovieInformation {
        repeat(2) { pass ->
            processRequiredProperties(pass)
            processProperty(titleProperty, pass)
            processProperty(yearProperty, pass)
        }

        return buildEpisodeInformation()
    }

    private fun processProperty(property: Property<*>, pass: Int) {
        property
            .process(this, pass)
            .sortedByDescending { it.quality }
            .firstOrNull()?.let {
                property.marked = true
                property.match = it
            }
    }

    private fun processRequiredProperties(pass: Int) {
        if (!titleProperty.marked) processProperty(titleProperty, pass)
    }

    private fun buildEpisodeInformation(): MovieInformation {
        return MovieInformation(
            titleProperty.match?.toString() ?: "",
            yearProperty.match?.result as Int?
        )
    }

    override fun matches(): List<MatchStats> {
        return listOfNotNull(
            titleProperty.match,
            yearProperty.match
        )
    }
}

data class MovieInformation(
    val title: String = "",
    val year: Int? = null
)

package com.headlessideas.inferit.movie

import com.headlessideas.inferit.properties.IntProperty
import com.headlessideas.inferit.properties.NameProperty

object MovieConfig {
    internal fun configYearProperty(title: NameProperty): IntProperty {
        val year = IntProperty()
        year.addExtract(Regex("\\x28(\\d{4})\\x29"), 80.0).positionAfter(title) // "(2002)"
        return year
    }

    internal fun configTitleProperty(): NameProperty {
        // The episode title
        val title = NameProperty()

        title.addModifier("length", 1)
        title.addExtract(Regex("^([^\\x28]+)", RegexOption.IGNORE_CASE), 10.0)
        title.addExtract(Regex("^([^\\x28]+)", RegexOption.IGNORE_CASE), 10.0)
        title.addExtract(Regex(" - (.+)", RegexOption.IGNORE_CASE), 41.0)
        title.addExtract(Regex(" - (.+) -", RegexOption.IGNORE_CASE), 45.0)
        title.addExtract(Regex(" - (.+)\$", RegexOption.IGNORE_CASE), 60.0)

        return title
    }
}

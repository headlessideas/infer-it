package com.headlessideas.inferit.util

internal fun String.titleize(): String {
    return "\\b(?=\\w)".toPattern().split(this).joinToString("", transform = String::capitalize)
}
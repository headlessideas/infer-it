package com.headlessideas.inferit.file

internal class FileClassifier(private val name: FileType) {
    private val containers = mutableSetOf<FileContainer>()

    fun getFileType(filename: String): List<FileInformation> {
        return containers.flatMap { container ->
            container
                .extensions
                .filter { filename.endsWith(it.name, true) }
                .map { ext -> FileInformation(name, container.title, ext.name, container.quality, ext.score) }
        }
    }

    fun addContainer(title: String, quality: Int = 0): FileContainer {
        val container = FileContainer(title, quality)
        containers.add(container)
        return container
    }
}

enum class FileType {
    VIDEO, INDEX, INFO, SUBTITLE, SHORTCUT, UNKNOWN
}

data class FileInformation(
    val fileType: FileType,
    val container: String,
    val extension: String,
    val quality: Int,
    val score: Int
)
package com.headlessideas.inferit.file

internal class FileContainer(val title: String, val quality: Int) {
    val extensions = mutableListOf<FileExtension>()

    fun addExtension(name: String, score: Int = 10): FileContainer {
        extensions.add(FileExtension(name, score))
        return this
    }

    fun addExtensions(names: List<String>, score: Int = 10): FileContainer {
        extensions.addAll(names.map { FileExtension(it, score) })
        return this
    }
}

internal data class FileExtension(val name: String, val score: Int)
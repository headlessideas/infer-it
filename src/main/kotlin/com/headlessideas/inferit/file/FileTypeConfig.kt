package com.headlessideas.inferit.file

internal fun configFileTypes(): Set<FileClassifier> {
    return mutableSetOf(
        configVideoFileType(),
        configIndexFileType(),
        configInfoFileType(),
        configShortcutFileType(),
        configSubtitleFileType()
    )
}

private fun configVideoFileType(): FileClassifier {
    val video = FileClassifier(FileType.VIDEO)

    // Add the video file type
    video.addContainer("Real Media File", -50).addExtension("rm")
    video.addContainer("3GPP", -40).addExtensions(listOf("3g2", "3gp", "3gp2"))
    video.addContainer("MPEG Video File", 0).addExtensions(listOf("mpg", "mpeg"))
    video.addContainer("MPEG-2 Transport Stream", 10).addExtension("m2ts")
    video.addContainer("Video Transport Stream File", 10).addExtension("ts")
    video.addContainer("Flash Video", 10).addExtension("flv")
    video.addContainer("Advanced Systems Format", 10).addExtension("asf")
    video.addContainer("Audio Video Interleave", 20).addExtension("avi")
    video.addContainer("DivX Media Format", 20).addExtension("divx")
    video.addContainer("QuickTime File Format", 20).addExtension("mov").addExtension("qt", 1)
    video.addContainer("Ogg Media File", 30).addExtension("ogm")
    video.addContainer("Ogg Video File", 30).addExtension("ogv")
    video.addContainer("M4V", 30).addExtension("m4v")
    video.addContainer("Windows Media Video File", 30).addExtension("wmv")
    video.addContainer("MPEG-4 Part 14", 40).addExtension("mp4")
    video.addContainer("WebM Video File", 40).addExtension("webm")
    video.addContainer("Matroska", 50).addExtension("mkv")

    return video
}

private fun configIndexFileType(): FileClassifier {
    // add the fileindex file type
    val index = FileClassifier(FileType.INDEX)

    index.addContainer("BitTorrent").addExtension("torrent")
    index.addContainer("NewzBin Usenet Index File").addExtension("nzb")

    return index
}

private fun configInfoFileType(): FileClassifier {
    // Add the base info file type
    val info = FileClassifier(FileType.INFO)

    info.addContainer("Markdown").addExtension("md")
    info.addContainer("Plain Text").addExtension("txt")
    info.addContainer("Log").addExtension("log")

    return info
}

private fun configShortcutFileType(): FileClassifier {
    // Add shortcut file type
    val shortcut = FileClassifier(FileType.SHORTCUT)

    shortcut.addContainer("Desktop Entry File").addExtension("desktop")
    shortcut.addContainer("Internet Shortcut").addExtension("url")
    shortcut.addContainer("Windows File Shortcut").addExtension("lnk")

    return shortcut
}

private fun configSubtitleFileType(): FileClassifier {
    // Add subtitle filetype
    val subtitle = FileClassifier(FileType.SUBTITLE)
    subtitle.addContainer("SubRip Subtitle File").addExtension("srt")

    return subtitle
}
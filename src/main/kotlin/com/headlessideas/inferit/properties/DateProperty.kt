package com.headlessideas.inferit.properties

import java.time.LocalDate
import java.time.format.DateTimeFormatter

internal class DateProperty : Property<LocalDate>() {
    override fun parse(value: String): LocalDate? {
        var result = value
        if (!result.contains('-') && !result.contains('.')) {
            result = result.slice(0..3) + "-" + result.slice(4..5) + "-" + result.slice(6..7)
        }

        if ("[-.]\\d{4}$".toRegex().find(result) != null) {
            result = result.takeLast(4) + '-' + result.takeLast(7).dropLast(4) + '-' + result.slice(0..2)
        }

        result = result.replace('.', '-')

        val format = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        return LocalDate.parse(result, format)
    }
}
package com.headlessideas.inferit.properties

import com.headlessideas.inferit.AbstractParser
import com.headlessideas.inferit.Match
import com.headlessideas.inferit.MatchStats
import kotlin.math.min

internal abstract class Property<T> {
    private val matchers = mutableListOf<Match>()
    private val modifiers = mutableListOf<Modifier>()
    var match: MatchStats? = null
    var marked = false

    fun addExtract(regex: Regex, score: Double): Match {
        val match = Match(this, regex, score)
        matchers.add(match)
        return match
    }

    fun addMatch(value: Any, regex: Regex, score: Double = 10.0): Match {
        val match = Match(this, regex, score, value)
        matchers.add(match)
        return match
    }

    fun addModifier(type: String, modifier: Int): Property<T> {
        modifiers.add(Modifier(type, modifier))
        return this
    }

    abstract fun parse(value: String): T?

    fun process(parser: AbstractParser, pass: Int): List<MatchStats> {
        val results = mutableListOf<MatchStats>()

        for (matcher in matchers) {
            val beforeProperty = matcher.positionBeforeProperty
            val afterProperty = matcher.positionAfterProperty

            // If this is a second pass, but this match does not require anything special,
            // it was not meant for a multi pass
            if (pass > 1 && (matcher.requireProperty == null && beforeProperty == null && afterProperty == null)) continue

            // If this is a multi pass and this item has already been found, continue
            if (pass > 1 && match != null) continue

            // Make sure required properties are found
            val stats = matcher.requireProperty?.match
            if (matcher.requireProperty != null && stats == null) continue

            // If property values need to match, check it here
            if (matcher.requireProperty != null && matcher.requirePropertyValue != null && stats?.result != matcher.requirePropertyValue) continue

            // If this match should be before or behind another property, but not require it,
            // check that now
            if (matcher.requireProperty == null) {
                if (beforeProperty != null && !beforeProperty.marked) continue
                if (afterProperty != null && !afterProperty.marked) continue
            }

            // Begin with the source
            var source = parser.source

            beforeProperty?.let {
                source = getBefore(it, parser, source)
            }

            var toEdge = false
            if (afterProperty != null) {
                val after = getAfter(afterProperty, parser, source)
                toEdge = source.length == parser.source.length
                source = after
            }

            val temp = matcher.findMatch(source, toEdge) ?: continue

            for (modifier in modifiers) {
                when (modifier.type) {
                    "length" -> {
                        if (temp.toString().isNotEmpty()) {
                            val s = (1 + (min(35, temp.toString().length) / 50)) * modifier.modifier
                            temp.score *= s
                        }
                    }
                }
            }

            temp.extract?.apply {
                temp.score += length
            }

            temp.matcher = matcher
            results.add(temp)
        }

        return results.toList()
    }

    private fun getBefore(property: Property<*>, parser: AbstractParser, source: String): String {
        val stats = property.match ?: return source
        val matches = stats.matches

        var newEnd = matches.range.first

        for (stat in parser.matches()) {
            if (stat.begin > newEnd) continue
            if (stat.end == newEnd) {
                newEnd = stat.begin
                break
            }
        }

        return source.slice(0 until newEnd)
    }

    private fun getAfter(property: Property<*>, parser: AbstractParser, source: String): String {
        val match = property.match ?: return source

        var newBegin = match.end + 1
        var newEnd = source.length

        for (range in usedRanges(parser)) {
            // Skip ranges before the current wanted begin point
            if (range.end < newBegin) {
                continue
            }

            // As soon as a new range is found, stop there
            if (range.begin > newBegin) {
                newEnd = range.begin
                break
            }

            if (match.begin >= range.begin && match.end <= range.end) {
                newBegin = range.end + 1
            }
        }

        return source.slice(newBegin until newEnd)


    }

    private fun usedRanges(parser: AbstractParser): List<Range> {
        val ranges = mutableListOf<Range>()

        for (match in parser.matches()) {
            ranges.add(Range(match.begin, match.end))
        }
        ranges.sortBy { it.begin }

        return ranges.toList()
    }
}

private data class Range(val begin: Int, val end: Int)

private data class Modifier(val type: String, val modifier: Int)
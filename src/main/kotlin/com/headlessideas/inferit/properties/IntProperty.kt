package com.headlessideas.inferit.properties

internal class IntProperty : Property<Int>() {
    override fun parse(value: String): Int? {
        return value.toIntOrNull()
    }
}
package com.headlessideas.inferit.properties

import com.headlessideas.inferit.util.titleize

internal class NameProperty : Property<String>() {
    override fun parse(value: String): String? {
        var result = value.trim()

        if (result.indexOf('(') > 0) {
            result = result.substringBefore('(')
        }

        if (result.indexOf('[') > 0) {
            result = result.substringBefore('[')
        }

        result = result.substringAfterLast(']')

        //todo: normalizeAcronyms (removes dots from acronyms)
        result = result
            .replace("-\$".toRegex(), "")
            .replace("^-".toRegex(), "")
            .replace(" - ".toRegex(), "")
            .replace("[._]".toRegex(), " ")
            .trim()
        result = result.toLowerCase()

        //See if the title is jumbled
        val temp = result.split(',')

        if (temp.size == 2) {
            val pc = temp[1].trim()

            if (pc.length < 5 && !pc.contains(' ')) {
                result = pc + ' ' + temp[0]
            }
        }

        return result.titleize()
    }
}
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.50"
    id("com.github.ben-manes.versions") version "0.22.0"
    id("com.jfrog.bintray") version "1.8.4"
    `maven-publish`
}

apply {
    from("keystore.gradle")
}


group = "com.headlessideas"
version = "1.1-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    compile(kotlin("stdlib-jdk8"))
    implementation("org.slf4j:slf4j-api:1.7.25")

    testImplementation("org.slf4j:slf4j-simple:1.7.25")
}

val sourcesJar by tasks.registering(Jar::class) {
    classifier = "sources"
    from(sourceSets.main.get().allSource)
}

bintray {
    val bintrayKey: String by extra
    user = "nandi"
    key = bintrayKey
    publish = true
    override = true

    setPublications("infer-it")

    with(pkg) {
        repo = "maven"
        name = "infer-it"
        vcsUrl = "https://gitlab.com/headlessideas/infer-it"
        setLicenses("MIT")
        with(version) {
            name = project.version.toString().removeSuffix("-SNAPSHOT")
        }
    }

}

publishing {
    repositories {
        maven {
            // change to point to your repo, e.g. http://my.org/repo
            url = uri("https://dl.bintray.com/nandi/infer-it")
        }
    }
    publications {
        register("infer-it", MavenPublication::class) {
            groupId = group as String
            artifactId = rootProject.name
            version = version.removeSuffix("-SNAPSHOT")

            from(components["java"])
            artifact(sourcesJar.get())
        }
    }
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

tasks.withType<Jar> {
    manifest {
        attributes["Main-Class"] = "AppKt"
    }
}